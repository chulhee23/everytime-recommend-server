from scipy import sparse
import time
import pandas as pd
import numpy as np
from scipy import sparse
import pickle
from pathlib import Path
import os

try:
    parent = Path(__file__).parent
except:
    parent = Path(os.getcwd())

def fileName(file):
    return parent/file

class similar(object):
    def __init__(self):

        # 기본 데이터 적재
        self.detail_raw = pd.read_csv(fileName('detail.csv'))
        self.review_raw = pd.read_csv(fileName('review1104.csv'))

        # doc2vec 모델 결과 적재
        self.doc_similarity_lecture = np.load(fileName("d2v_lecture_sim.npy"))
        self.doc_similarity_prof = np.load(fileName("d2v_prof_sim.npy"))
        self.doc_tags_lecture = np.load(fileName("d2v_lecture_tag.npy"))
        self.doc_tags_prof = np.load(fileName("d2v_prof_tag.npy"))
        self.doc_token_lecture = pd.read_csv(fileName('d2v_lecture_token.csv'))
        self.doc_token_prof = pd.read_csv(fileName('d2v_prof_token.csv'))

        # TF-IDF 모델 결과 적재
        self.tf_data_lecture=pd.read_csv(fileName('tf_data_lecture.csv'))
        self.cos_mat_lecture=np.load(fileName('tf_cos_mat_lecture.npy') )
        with open(fileName('tf_hashs_lecture.pickle'), 'rb') as f:
            self.hash_lecture = pickle.load(f)
        self.tfidf_matrix_tag_lecture = np.load(fileName('tf_matrix_tag_lecture.npy'))
        with open(fileName('tf_tok_corpus_lecture.pickle'), 'rb') as f:
            self.tok_corpus_lecture = pickle.load(f)

        self.tf_data_prof=pd.read_csv(fileName('tf_data_prof.csv'))
        self.cos_mat_prof=np.load(fileName('tf_cos_mat_prof.npy'))
        with open(fileName('tf_hashs_prof.pickle'), 'rb') as f:
            self.hash_prof = pickle.load(f)
        self.tfidf_matrix_tag_prof = np.load(fileName('tf_matrix_tag_prof.npy'))
        with open(fileName('tf_tok_corpus_prof.pickle'), 'rb') as f:
            self.tok_corpus_prof = pickle.load(f)

    # 강의,교수 input
    def cli_question(self):
        check = input('강의면 Y, 교수면 N(기본값) : ')
        if check == 'Y' :
            name = input('강의 이름을 입력해 주세요 : ')
            prof = input('교수 이름을 입력해 주세요 : ')
            return self.find_similar_lecture(name, prof)
        else:
            prof = input('교수 이름을 입력해 주세요 : ')
            return self.find_similar_prof(prof)

    def find_similar_lecture(self, name, prof):

        sim = self.doc_similarity_lecture
        tags = self.doc_tags_lecture
        token = self.doc_token_lecture

        tf_data = self.tf_data_lecture
        cos_mat = self.cos_mat_lecture

        hashs = self.hash_lecture
        tfidf_matrix_tag = self.tfidf_matrix_tag_lecture

        tok_corpus = self.tok_corpus_lecture

        tag = '-'.join([name,prof])

        doc_sim_idx = np.argsort(sim[np.where(tags == tag)][0])[::-1][1:10]


        idx = tf_data.query('name == @name & prof == @prof').index[0]
        nouns = np.argsort(tfidf_matrix_tag[idx,:])[::-1][:5]

        hash_tags=[]
        for hash_tag in nouns :
            hash_tags.append(hashs[hash_tag])

        in_ = tok_corpus[idx]
        sim_idx = np.argsort(cos_mat[idx,:])[::-1][1:50]
        jac_sim={}

        for idxs in sim_idx:
            compute_ = tok_corpus[idxs]
            union = set(in_).union(set(compute_))
            intersection = set(in_).intersection(set(compute_))
            jac_sim[idxs] = len(intersection)/len(union)

        out_idx = (pd.Series(jac_sim).sort_values(ascending=False)[:10]).index

        d2v_result = token.loc[doc_sim_idx,['name','prof','review']]
        tf_result = tf_data.loc[out_idx,['name','prof','review']]

        result = pd.concat([tf_result,d2v_result]).drop_duplicates()
        detail = dict(self.detail_raw.query('name==@name & prof ==@prof').iloc[0,:5])
        review = self.review_raw.query('name==@name & prof == @prof').iloc[:,:2].values

        output = {
            'name' : name, 'prof' : prof , 'hash_tags' : hash_tags , 'detail' : detail,
            'similar' : result.values, 'review': review
                 }
        return output


    def find_similar_prof(self, prof):

        sim = self.doc_similarity_prof
        tags = self.doc_tags_prof
        token = self.doc_token_prof

        tf_data = self.tf_data_prof
        cos_mat = self.cos_mat_prof

        hashs = self.hash_prof
        tfidf_matrix_tag = self.tfidf_matrix_tag_prof

        tok_corpus = self.tok_corpus_prof

        idx = tf_data.query('prof == @prof').index[0]
        tag = prof
        nouns = np.argsort(tfidf_matrix_tag[idx,:])[::-1][:5]

        hash_tags=[]
        for hash_tag in nouns :
            hash_tags.append(hashs[hash_tag])

        in_ = tok_corpus[idx]
        sim_idx = np.argsort(cos_mat[idx,:])[::-1][1:50]
        jac_sim={}

        for idxs in sim_idx:
            compute_ = tok_corpus[idxs]
            union = set(in_).union(set(compute_))
            intersection = set(in_).intersection(set(compute_))
            jac_sim[idxs] = len(intersection)/len(union)

        out_idx = (pd.Series(jac_sim).sort_values(ascending=False)[:10]).index
        doc_sim_idx = np.argsort(sim[np.where(tags == tag)][0])[::-1][1:9]

        d2v_result = token.loc[doc_sim_idx,['prof','review']]
        tf_result = tf_data.loc[out_idx,:]

        result = pd.concat([tf_result,d2v_result]).drop_duplicates()
        result = result.sample(frac=1).reset_index(drop=True)
        detail = dict(self.detail_raw.query('prof == @prof').iloc[:,:5].apply(lambda x:x.mode()).iloc[0,:])
        review = self.review_raw.query('prof == @prof').iloc[:,:3].values



        output = {
            'prof' : prof , 'hash_tags' : hash_tags , 'detail' : detail,
                  'similar' : result.values, 'review' : review
                }
        return output


if __name__ == '__main__':
    test = similar()
    a = test.cli_question()
    print(a)